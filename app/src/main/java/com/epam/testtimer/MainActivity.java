package com.epam.testtimer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private static final long DELAY = 10000;
    static String URL = "http://diva-decors.com/orion.php?realOffset=";
    private OkHttpClient client;
    public static Runnable mTask;

    CustomThread thread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        client = new OkHttpClient();
        startTimer();
    }


    private void startTimer() {
        mTask = getRunnable();
        mTask.run();
        thread = new CustomThread();
//        thread.setPriority(Thread.MIN_PRIORITY);
        thread.start();
    }

    long lastCall;

    @NonNull
    private Runnable getRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                Log.d("TIMER_TEST", "task execute called");
                long l = System.currentTimeMillis();
                final long realOffset = l - lastCall;
                lastCall = l;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String url = URL+realOffset;
                        Request request = new Request.Builder()
                                .url(url)
                                .build();

                        Response response = null;
                        try {
                            response = client.newCall(request).execute();
                            String string = response.body().string();
                            Log.d("TIMER_TEST", "task execute done");
                        } catch (IOException e) {
                            e.printStackTrace();

                        }
                    }
                }).start();
            }
        };
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d("TIMER_TEST", "activity paused");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finishTimer();
        client = null;
    }

    private void finishTimer() {
        mTask = null;
        thread.cancel();
        thread = null;
    }

    private class CustomThread extends Thread {

        long offset = 30000;
        boolean cancel;

        public void setOffset(long offset) {
            this.offset = offset;
        }

        public void cancel() {
            this.cancel = true;
        }

        @Override
        public void run() {
            while (!cancel) {
                try {
                    Thread.sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                long l = System.currentTimeMillis();
                if ((l - lastCall) < (offset - 1000)) {
                    continue;
                }
                if (!cancel && mTask != null) {
                    mTask.run();
                }
            }
        }
    }
}
